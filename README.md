![cmp](docs/cmp_logo.png)

A study of an interpreter handling
measurement program definitions
(**mpd**) written in [clojure](https://clojure.org/).

See documentation on [wactbprot.github.io](https://wactbprot.github.io/cmp/)

